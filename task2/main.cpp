#include <iostream>
#include <cstring>
#include <bitset>
#include <string>
#include <limits>

const char * toBinaryMode = "--toBinary";
const char * toIntMode = "--toInt";
const int binaryWidth = std::numeric_limits<unsigned long long>::digits;

int main(int argc, char ** argv) {
	if(argc == 1 || !strcmp(argv[1],toIntMode)) {
		std::string binaryData;
		std::cin >> binaryData;
		std::bitset<binaryWidth> tmp(binaryData);
		std::cout << tmp.to_ullong();
	}
	else if(!strcmp(argv[1],toBinaryMode)) {
		unsigned long long decimalData;
		std::cin >> decimalData;
		std::bitset<binaryWidth> tmp(decimalData);
		auto holder = tmp.to_string();	
		if(decimalData) std::cout << holder.substr(holder.find_first_of('1'));
		else std::cout << 0; 
	}
	else {
		std::cerr << "Invalid mode";
	}
	return 0;
}
