#include <chrono>
#include <initializer_list>
#include <vector>
#include <string>
#include <random>
#include <thread>
#include <ncurses.h>

//Base sensor class - every sensor supplied to carTelemetry must be derived from it
class basicSensor {
	public: 
		//Useful when extending ncurses window formatting
		enum group {steer, engine, wheels, general};
		
		basicSensor(std::string unit, std::string name, group type): 
			unit(unit), name(name), type(type) {}

		std::string& getUnit() {
			return unit;
		};
		
		std::string& getName() {
			return name;
		}

		group getGroup() {
			return type;
		}
		
		//Must be overwritten in a derived class
		//getDataF stands for get formatted data - useful for defining formatting rules specific to a sensor.
		//virtual is useful here because it lets you add and remove sensor from carTelemetry at runtime.
		virtual std::string& getDataF()=0;
		virtual long double& getData()=0;

	protected:
		long double data;
		std::string dataF;
		std::string unit;
		std::string name;
		group type;
};

//Example sensor class supplying randomized data - imitates real world sensor 
class exampleSensor: public basicSensor {
	public:
		exampleSensor(std::uniform_real_distribution<long double> dist, std::mt19937& gen, std::string unit, std::string name, basicSensor::group type): 
			dist(dist), basicSensor(unit,name,type), gen(gen) {}

		std::string& getDataF() {
			getData();
			dataF = std::to_string(data)+unit;
			return dataF;
		}

		long double& getData() {
			data = dist(gen);
			return data;
		}

	protected:
		std::mt19937 gen;
		//Used to store the value range to imitate real world sensor data
		std::uniform_real_distribution<long double> dist;
};

//Car telemetry logging using the ncurses library. 
class carTelemetry {
	public:
		carTelemetry(unsigned pollingInterval, std::initializer_list<basicSensor*> sensorList):
			pollingInterval(pollingInterval), sensorList(sensorList), minPollingInterval(1) {}

		bool updatePollingInterval(int pI) {
			if(std::chrono::milliseconds(pI) < minPollingInterval)
			pollingInterval = std::chrono::milliseconds(pI);
			return true;
		}
		
		//Method that fetches the data from the sensors and updates the screen periodically 
		//Makes it possible to launch the logging in a separate thread
		//Sets minPollingInterval depending on a wait time to fetch the data from the sensors 
		//If minPollingInterval is larger than the current pollingInterval - updates it to the value of minPollingInterval 
		void operator()() {
			initscr();
			auto begin = std::chrono::high_resolution_clock::now();
			auto end = begin;
			std::chrono::milliseconds tmp;
			for(;;) {
				begin = std::chrono::high_resolution_clock::now();
				erase();
				for(auto &i: sensorList) {
					printw((i->getName()+": "+i->getDataF()+"\n").c_str());
				}
				printw(("\nPolling interval: " + std::to_string(pollingInterval.count()) + "ms").c_str());
				refresh();
				end = std::chrono::high_resolution_clock::now();
				tmp = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin);
				minPollingInterval = tmp+std::chrono::milliseconds(1);
				if(pollingInterval<minPollingInterval) pollingInterval = minPollingInterval;
				std::this_thread::sleep_for((pollingInterval)-tmp);
			}
		}

	private:
		//Polling interval expressed in milliseconds
		std::chrono::milliseconds pollingInterval;
		std::chrono::milliseconds minPollingInterval;
		std::vector<basicSensor*> sensorList;

};

//Example driver program
int main() {
	typedef std::uniform_real_distribution<long double> realDist;
	typedef basicSensor::group group;
	std::random_device rd;
	std::mt19937 gen(rd());
	exampleSensor engineTemp(realDist(70,170), gen, "C", "engine temp", group::engine);
	exampleSensor steerAngle(realDist(-180,180), gen, "deg", "steer angle", group::steer);
	exampleSensor velocity(realDist(0,200), gen, "kmh", "velocity", group::general);
	carTelemetry loggingDevice(1000, {&engineTemp, &steerAngle, &velocity});
	loggingDevice();
	return 0;
}

