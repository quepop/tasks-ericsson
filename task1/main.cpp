#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <filesystem>

const int objectSize = 8;
const char inputFilename[] = "input.txt";
const char outputFilename[] = "output.txt";
const char fileErrMsg[] = "Failed to open a file: ";
const char badModeErrMsg[] = "Selected optimization mode does not exist";
const char badDataErrMsg[] = "Invalid input data";
std::ifstream inputFile;
std::ofstream outputFile;

inline void kill(int &&exitCode) {
	inputFile.close();
	outputFile.close();
	exit(exitCode);
}

inline bool getNextObject(std::ifstream &src, char &dst) {
	char c = src.peek();
	if((c != '1' && c != '0')) return false; 
	dst = 0b00000000;
	for(size_t i = 0; i < objectSize; ++i) {
		src.get(c);
		if((c != '1' && c != '0')) {
			std::cerr << badDataErrMsg << '\n';
			kill(EXIT_FAILURE);
		}
		dst <<= 1;
		dst |= (c-'0');
	}
	return true;
}

inline void writeNumToBin(std::ofstream &dst, char &src) {
	for(size_t i = 0; i < objectSize; ++i) dst << ((src >> (objectSize-1-i))&1);
}

inline bool checkIfValid(char &src) {
	return ( (((src >> 1) & 0b111) != 0b000) && ((src >> 4)%2 == (src & 0b1)) );
}

int main(int argc, char ** argv) {

	char dst, mode = 's';
	size_t total = 0, totalErr = 0;
	size_t fileSize = std::filesystem::file_size(std::filesystem::path(inputFilename))/objectSize;
	std::vector<char> tmpBuffer(0);

	inputFile.open(inputFilename);
	outputFile.open(outputFilename);

	if(!inputFile.good()) std::cerr << fileErrMsg << inputFilename << '\n'; 
	if(!outputFile.good()) std::cerr << fileErrMsg << outputFilename << '\n';
	if(!inputFile.good() || !outputFile.good()) kill(EXIT_FAILURE);
	
	for(size_t i = 1; i < argc; ++i) {
		if(argv[i][0] != '-' || argv[i][1] == '\0' || argv[i][2] != '\0') continue;
		switch(argv[i][1]) {
			case 'O':
				mode = (((i+1) >= argc) || (argv[i+1][1] != '\0')) ? '\0' : argv[i+1][0];
				++i;
				break; 
		}
	}
	
	switch(mode) {
		case 'm':
			while(getNextObject(inputFile, dst)) {
				++total;
				if(!checkIfValid(dst)) ++totalErr;
			}
			inputFile.seekg(0);
			outputFile << total << '\n' << totalErr << '\n';
			while(getNextObject(inputFile, dst)) if(checkIfValid(dst)) writeNumToBin(outputFile, dst);    
			break;
		case 's':
			tmpBuffer.reserve(fileSize);
			while(getNextObject(inputFile, dst)) {
				++total;
				if(checkIfValid(dst)) tmpBuffer.push_back(dst);
				else ++totalErr;
			}
			outputFile << total << '\n' << totalErr << '\n';
			for(auto &i: tmpBuffer) writeNumToBin(outputFile, i);
			break;
		default:
			std::cerr << badModeErrMsg << '\n';
			kill(EXIT_FAILURE);
	}
	
	kill(EXIT_SUCCESS);
}
