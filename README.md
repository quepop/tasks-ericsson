# task1
Checks for errors in the supplied data and in the command line/launch parameters - prints appropriate message on stderr. There is one possible command line parameter "-O" which modifies how the program executes the algorithm. "-O m" optimizes memory usage - doesnt load the entire input file into memory but uses I/O buffers (reads the input file twice). "-O s" optimizes speed - loads the entire input file into memory (reads the input file once). If the "-O" parameter isnt set the program will behave as if "-O s" was set. Structure of the program makes it easy to add more command line parameters. You need a compiler that supports c++17.
Compilation command:
```
g++ main.cpp -std=c++17
```

# task2
launch/command line parameters:
* "--toBinary" - converts a number to binary.
* "--toInt" - converts a binary number to decimal. \
If a launch parameter isnt set the program defaults to "--toInt". \
Compilation command:
```
g++ main.cpp
```

# task3
The program uses ncurses library. To compile it install ncurses-dev package and use the command below:
```
g++ -lncurses main.cpp
```
